<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bairro extends Model
{
	protected $table = 'BAIRROS';
    protected $fillable = ['COD_BAIRRO', 'BAIRRO'];
    

    // protected $dates = ['deleted_at'];    
}
