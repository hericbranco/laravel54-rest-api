<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

use JWTAuth;
use Hash;

class AuthController extends Controller
{
	public function authenticate() {

    	//request()
		$arrCredentials = request()->only('email', 'password');
		$user = User::where('email', $arrCredentials['email'])->first();
		if(!$user) {
			return response()->json([
				'error' => 'Invalid credentials'
			], 401);
		}

		if (!Hash::check($arrCredentials['password'], $user->password)) {
			return response()->json([
				'error' => 'Invalid credentials'
			], 401);
		}

		$token = JWTAuth::fromUser($user);

		$objectToken = JWTAuth::setToken($token);
		$expiration = JWTAuth::decode($objectToken->getToken())->get('exp');

		return response()->json([
			'access_token' => $token,
			'token_type' => 'bearer',
			'expires_in' => $expiration
		]);

	}
}
