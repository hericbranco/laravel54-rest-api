<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group(['middleware' => 'auth:api'], function () {
	Route::get('/users', function(){echo'dsa';return 'dsa';exit;});
});

Route::get('/users', 'UsersController@index');
Route::resource('jobs', 'JobsController');
Route::resource('companies', 'CompaniesController');

Route::post('auth/login', 'AuthController@authenticate');